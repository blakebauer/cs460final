﻿CREATE TABLE dbo.Buyers (
	BuyerID		INT				IDENTITY (1,1) NOT NULL,
	Name		NVARCHAR(255)	NOT NULL,
	CONSTRAINT	PK_Buyer		PRIMARY KEY CLUSTERED (BuyerID ASC)
);

CREATE TABLE dbo.Sellers (
	SellerID		INT				IDENTITY (1,1) NOT NULL,
	Name			NVARCHAR(255)	NOT NULL,
	CONSTRAINT		PK_Seller		PRIMARY KEY CLUSTERED (SellerID ASC)
);

CREATE TABLE dbo.Items (
	ItemID			INT				IDENTITY (1,1) NOT NULL,
	SellerID		INT				NOT NULL,
	Name			NVARCHAR(255)	NOT NULL,
	Description		TEXT			NOT NULL,
	CONSTRAINT		PK_Item			PRIMARY KEY CLUSTERED (ItemID ASC),
	CONSTRAINT		FK_SellerItem	FOREIGN KEY (SellerID) REFERENCES Sellers(SellerID)
);

CREATE TABLE dbo.Bids (
	BidID			INT				IDENTITY (1,1) NOT NULL,
	ItemID			INT				NOT NULL,
	BuyerID			INT				NOT NULL,
	Price			DECIMAL			NOT NULL,
	Timestamp		DATETIME		NOT NULL,
	CONSTRAINT		PK_Bid			PRIMARY KEY CLUSTERED (BidID ASC),
	CONSTRAINT		FK_ItemBid		FOREIGN KEY (ItemID) REFERENCES Items(ItemID),
	CONSTRAINT		FK_BuyerBid		FOREIGN KEY (BuyerID) REFERENCES Buyers(BuyerID)
);

INSERT INTO dbo.Buyers (Name) VALUES 
	('Jane Stone'),
	('Tom McMasters'),
	('Otto Vanderwall');

INSERT INTO dbo.Sellers (Name) VALUES 
	('Gayle Hardy'),
	('Lyle Banks'),
	('Pearl Greene');

INSERT INTO dbo.Items (SellerID, Name, Description) VALUES 
	(3, 'Abraham Lincoln Hammer'    ,'A bench mallet fashioned from a broken rail-splitting maul in 1829 and owned by Abraham Lincoln'),
	(1, 'Albert Einsteins Telescope','A brass telescope owned by Albert Einstein in Germany, circa 1927'),
	(2, 'Bob Dylan Love Poems'      ,'Five versions of an original unpublished, handwritten, love poem by Bob Dylan');

INSERT INTO dbo.Bids (BuyerID, ItemID, Price, Timestamp) VALUES 
	(3, 1, 250000,'12/04/2017 09:04:22'),
	(1, 3, 95000 ,'12/04/2017 08:44:03');
