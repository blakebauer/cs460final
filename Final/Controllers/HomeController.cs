﻿using Final.DAL;
using Final.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Final.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Bid()
        {
            using(AuctionContext db = new AuctionContext())
            {
                ViewBag.BuyerID = new SelectList(db.Buyers.ToList(), "BuyerID", "Name");
                ViewBag.ItemID = new SelectList(db.Items.ToList(), "ItemID", "Name");
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Bid([Bind(Include = "BidID,BuyerID,ItemID,Price")] Bid bid)
        {
            using (AuctionContext db = new AuctionContext())
            {
                if (ModelState.IsValid)
                {
                    bid.Timestamp = DateTime.Today;
                    db.Bids.Add(bid);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.BuyerID = new SelectList(db.Buyers, "BuyerID", "Name", bid.BuyerID);
                ViewBag.ItemID = new SelectList(db.Items, "ItemID", "Name", bid.ItemID);
                return View(bid);
            }
        }

        public JsonResult ItemBids(int itemID)
        {
            using (AuctionContext db = new AuctionContext())
            {
                var stuff = new List<object>();
                var bids = db.Bids.Where(b => b.ItemID == itemID).OrderByDescending(b => b.Price).ToList();
                foreach (var bid in bids)
                {
                    stuff.Add(new { bid.Price, Buyer = bid.Buyer.Name });
                }
                return Json(stuff, JsonRequestBehavior.AllowGet);
            }
        }
    }
}