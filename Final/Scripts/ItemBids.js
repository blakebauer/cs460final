﻿var ajax_call = function () {
    var result = $.ajax({
        type: "GET",
        dataType: "json",
        url: "../../Home/ItemBids",
        data: {
            itemID: itemid
        }
    }).done(function () {
        $("#bids").empty()
        result.responseJSON.forEach(function (b) {
            $("#bids").append("<tr><td>" + b.Buyer + "</td><td>" + b.Price + "</td></tr>");
        });
    });
}

ajax_call();

var interval = 1000 * 5;

window.setInterval(ajax_call, interval);